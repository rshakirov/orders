<div class="btn-group dropdown cm-submit cm-check-items" style="float: left;">
   <a id="sw_{$id}_wrap" class="btn btn-info o-status-b btn dropdown-toggle cm-combination" data-toggle="dropdown">
      <span>Статус заказа</span>
      <span class="caret"></span>
   </a>
   <ul class="dropdown-menu">
      <li><a class="cm-on" {$check_data nofilter}>{__("check_all")}</a></li>
      <li><a class="cm-off" {$check_data nofilter}>{__("check_none")}</a></li>
      <li><a data-ca-status="p">{$order_status_descr['P']}</a></li>
      <li><a data-ca-status="c">{$order_status_descr['C']}</a></li>
      <li><a data-ca-status="o">{$order_status_descr['O']}</a></li>
      <li><a data-ca-status="f">{$order_status_descr['F']}</a></li>
      <li><a data-ca-status="d">{$order_status_descr['D']}</a></li>
      <li><a data-ca-status="b">{$order_status_descr['B']}</a></li>
      <li><a data-ca-status="i">{$order_status_descr['I']}</a></li>
      <li><a data-ca-status="y">{$order_status_descr['Y']}</a></li>
      <li><a data-ca-status="x">{$order_status_descr['X']}</a></li>
      <li><a data-ca-status="w">{$order_status_descr['W']}</a></li>
      <li><a data-ca-status="a">{$order_status_descr['A']}</a></li>
      <li><a data-ca-status="e">{$order_status_descr['E']}</a></li>
      {foreach $check_statuses as $status => $title}
      <li><a {$check_data nofilter} data-ca-status="{$status|lower}">{$title}</a></li>
      {/foreach}
   </ul>
</div>
<div class="cm-popup-box dropdown" style="float: left;">
   <a id="sw_{$id}_wrap" class="btn btn-info o-status-c btn dropdown-toggle cm-combination" data-toggle="dropdown">
      <span>Изменить статус</span>
      <span class="caret"></span>
   </a>
   <ul class="dropdown-menu">
      <li><a class="status-link-p cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..P]">{$order_status_descr['P']}</a></li>
      <li><a class="status-link-c cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..C]">{$order_status_descr['C']}</a></li>
      <li><a class="status-link-o cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..O]">{$order_status_descr['O']}</a></li>
      <li><a class="status-link-f cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..F]">{$order_status_descr['F']}</a></li>
      <li><a class="status-link-d cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..D]">{$order_status_descr['D']}</a></li>
      <li><a class="status-link-b cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..B]">{$order_status_descr['B']}</a></li>
      <li><a class="status-link-i cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..I]">{$order_status_descr['I']}</a></li>
      <li><a class="status-link-y cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..Y]">{$order_status_descr['Y']}</a></li>
      <li><a class="status-link-x cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..X]">{$order_status_descr['X']}</a></li>
      <li><a class="status-link-w cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..W]">{$order_status_descr['W']}</a></li>
      <li><a class="status-link-a cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..A]">{$order_status_descr['A']}</a></li>
      <li><a class="status-link-e cm-submit cm-post" data-ca-target-id="orders_total,pagination_contents" data-ca-target-form="orders_list_form" data-ca-dispatch="dispatch[change_select.status_change..E]">{$order_status_descr['E']}</a></li>
      <li class="divider"></li>
      <li>
         <a>
            <label for="{$id}_notify">
               <input type="checkbox" name="__notify_user" id="{$id}_notify" value="Y" checked="checked" onclick="Tygh.$('input[name=__notify_user]').prop('checked', this.checked);" />
               {__("notify_customer")}
            </label>
         </a>
         <a>
            <label for="{$id}_notify_department">
               <input type="checkbox" name="__notify_vendor" id="{$id}_notify_department" value="Y" checked="checked" onclick="Tygh.$('input[name=__notify_department]').prop('checked', this.checked);" />
               {__("notify_orders_department")}
            </label>
         </a>
      </li>
   </ul>
</div>